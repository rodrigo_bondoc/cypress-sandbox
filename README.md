# Cypress Sandbox

## Important Notes
1. HTML id attributes change when a version is changed
    - Referencing elements by their ID will not always work, since they change between versions
    - Having a data-test='something' attribute is required

## Starting Cypress
```shell
> npm run cypress:open
```


## Steps to Writing a Real Test
1. Set up application state
2. Take action
3. Make an assertion about the resulting application state

## Resources
1. [Write a Real Test](https://docs.cypress.io/guides/getting-started/writing-your-first-test#Write-your-first-test)


## Writing Questionnaire Data to Files, for checking reports
1. Use .csv or .json?

## Things to Do

## Random Findings
1. In PIN screens, the [obj-name] attribute changes for each PIN key after a key is pressed 🙃 