interface PatientFormOptions {
    subjectId?: string
    firstName?: string
    lastName?: string
    language?: string
    email?: string
    site?: string
}

export default class MgmtUi {
    user: string
    pass: string
    isLoggedIn: boolean

    widgets: {[key: string]: string} = {
        patients: "#patients_link",
        users: "#users_link",
        sites: "#sites_link",
        roles: "#roles_link",
        siteVisits: "#site_visits_link",
        epros: "#epros_link",
        auditTrail: "#audit_trail_link",
        reports: "#reports_link",
        dataManagement: "#data_mgmt_link",
        deviceManagement: "#device_mgmt_link",
        telemedicine: "#telemedicine_link",
    }

    constructor(user: string, pass: string) {
        this.user = user
        this.pass = pass
        this.isLoggedIn = false
    }

    login() {
        cy.visit("/mgmt-ui/#/login")
        
        cy.get("input[name='email']").clear().type(this.user)

        cy.get("input[name='password']").clear().type(this.pass).then(function() {
            cy.get(".login-button").click()
        }.bind(this))        

        cy.url().should("include", "home")

        return this
    }

    createPatient(options?: PatientFormOptions) {

        cy.get(this.widgets.patients).should("be.visible").click().then(function() {
            cy.wait(1000)
            cy.get("#add-patient-button").should("be.visible").click()
        })

        cy.get(".cdk-overlay-container").should("be.visible")
        .within(function() {
            cy.get(".dialog-title").should("have.text", "Add Patient")
            .then(function() {

                cy.contains("Select a Language").should("be.visible").click()
                .then(function() {
                    if(options != undefined && options.language) {

                    } else {
                        return cy.contains("English (US)").click()
                    }
                })
                .then(function () {
                    if(options != undefined && options.site) {

                    } else {
                        return cy.contains("Auto").click()
                    }
                })
            })
        })

        return this
    }
}