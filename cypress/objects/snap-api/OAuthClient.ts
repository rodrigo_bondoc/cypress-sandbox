export default class OAuthClient {

    private url: string = "/snap-oauth/oauth/token"

    getToken(user: string, pass: string): Cypress.Chainable<JQuery<string>> {
        return cy.request({
            "method": "POST",
            "url": this.url,
            "form": true,
            "body": {
                username: user,
                password: pass,
                client_id: "snapClinicalClient",
                client_secret: "secret",
                grant_type: "password",
            },
            retryOnStatusCodeFailure: true,
        })
        .then(function(response) {
            const body = response.body
            expect(response.status).to.equal(200)
            
            return body.access_token
        })
    }

    getAnonToken(): Cypress.Chainable<JQuery<string>> {
        return this.getToken("forgetfulUser", "4mn3s14C!")
    }
}