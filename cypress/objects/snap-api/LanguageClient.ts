import OAuthClient from "./OAuthClient"

export default class LanguageClient {
    private oauth: OAuthClient

    constructor() {
        this.oauth  = new OAuthClient()
    }

    getAllLanguages(): Cypress.Chainable<JQuery<Object>> {
        return this.oauth.getAnonToken().then(function(token) {
            cy.request({
                method: "GET",
                retryOnStatusCodeFailure: true,
                url: "/snap-api/getAllLanguages",
                auth: {
                    "bearer": token,
                }
            })
            .then(function(response) {
                expect(response.status).to.equal(200)
    
                return response.body
            })
        })
        
    }

}