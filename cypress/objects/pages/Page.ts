export default class Page {
    /*
        @property pathTail
            Ex: https://pathway-dev2.snapclinical.net/studio/s3/apps/patientmainapp/?SMainTest

            The pathTail would be "SMainTest"
            The pathTail helps describe which page the app is currently displaying
    */
    protected pathTail: string
    
    constructor(pathTail: string) {
        this.pathTail = pathTail
    }

    assertIsCurrentPage(): this {
        cy.url().should("include", this.pathTail)

        return this
    }
}