import Page from "./Page"

export default class ErrorPage extends Page {
    protected errorSelector: string

    constructor(pathTail: string, error: string) {
        super(pathTail)
        this.errorSelector = error
    }

    error(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.errorSelector)
    }

    errorText(): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.error().invoke("text")
    }
}