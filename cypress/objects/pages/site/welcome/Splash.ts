import Page from "../../Page"

export default class SplashPage extends Page {
    private continueButtonSelector: string = "[obj-name='BMainScreenContinue']"

    constructor(pathTail: string) {
        super(pathTail)
    }

    continue() {
        this.assertIsCurrentPage()
        cy.get(this.continueButtonSelector).click()
        
        return this
    }
}