import ErrorPage from "../../ErrorPage"

export default class LoginPage extends ErrorPage {

    private usernameSelector: string = "[obj-name='TLoginEmail']"
    private passwordSelector: string = "[obj-name='TLoginPassword']"
    private submitButtonSelector: string = "[obj-name='BLoginNext']"

    constructor(pathTail) {
        super(pathTail, "[obj-name='LLoginError']")
    }

    private enterUsername(email: string) {
        this.assertIsCurrentPage()

        if(email != "" && email != null) {
            return cy.get(this.usernameSelector).click().clear().type(email)
        }

        return cy.get(this.usernameSelector)
    }

    private enterPassword(password: string) {
        this.assertIsCurrentPage()

        if(password != "" && password != null) {
            return cy.get(this.passwordSelector).click().clear().type(password)
        }

        return cy.get(this.passwordSelector)
    }

    private submitForm() {
        return cy.get(this.submitButtonSelector).click()
    }

    loginWith(email: string, password: string) {
        return this.enterUsername(email)
        .then(function() {
            return this.enterPassword(password)
            .then(function() {
                return this.submitForm()
            }.bind(this))
        }.bind(this))
    }
}