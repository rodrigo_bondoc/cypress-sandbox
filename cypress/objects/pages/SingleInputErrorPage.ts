import ErrorPage from "./ErrorPage"

type TSelectors = {
    inputSelector: string,
    errorSelector: string,
    submitButtonSelector: string
}

export default class SingleInputErrorPage extends ErrorPage {

    protected selectors: TSelectors

    constructor(pathTail: string, selectors: TSelectors) {
        super(pathTail, selectors.errorSelector)
        this.selectors = selectors
    }

    input(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.selectors.inputSelector)
    }

    submitButton(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.selectors.submitButtonSelector)
    }

    typeValue(value: number | string): Cypress.Chainable<JQuery<HTMLElement>> {
        let convertedValue = String(value)
        
        return this.input().click().clear().type(convertedValue)
    }

    enterValue(value: number | string): Cypress.Chainable<JQuery<HTMLElement>> {

        if(value === "" || value === null) {
            return this.submitButton().click()
        } else {
            return this.typeValue(value).then(function() {
                return this.submitButton().click()
            }.bind(this))
        }

    }

}