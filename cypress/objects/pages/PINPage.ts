import ErrorPage from "./ErrorPage"

export default class PINPage extends ErrorPage {

    protected kpContainer: string = "[obj-name='GVPinKeypad']"

    protected keypad: string[] = [
        "IPinNumber_lceid_22", 
        "IPinNumber_lceid_2", "IPinNumber_lceid_4", "IPinNumber_lceid_6",
        "IPinNumber_lceid_8", "IPinNumber_lceid_10", "IPinNumber_lceid_12",
        "IPinNumber_lceid_14", "IPinNumber_lceid_16", "IPinNumber_lceid_18",
        "IPinNumber_lceid_22"
    ]
    protected inputs: string[] = [
        "LPinInput_lcename_2", "LPinInput_lcename_5", "LPinInput_lcename_8", "LPinInput_lcename_11"
    ]

    protected backButtonSelector: string = "[obj-name='BPinBack']"

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='LPinErrorMessage']")
    }

    pinTextByIndex(idx: number): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(`[obj-name='${this.inputs[idx]}']`)
    }

    backButton(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.backButtonSelector)
    }

    clickKey(key: string): Cypress.Chainable<JQuery<HTMLElement>> {
        let keyNum = parseInt(key)
        expect(keyNum).to.be.within(0, 9)

        let id: string = this.keypad[keyNum]

        // PIN Keys are acquired by ID because their obj-name attributes change after being clicked
        return cy.get(`${this.kpContainer} #${id}`).click()
    }    

    enterPin(pin: string | number): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        var convertedPin = String(pin)

        expect(convertedPin.length).to.equal(4)

        return cy.get(this.kpContainer).should("be.visible").then(function($container) {

            var key = convertedPin.charAt(0)
            this.clickKey(key).then(function($key) {
                this.pinTextByIndex(0).should("contain.text", "*")
            }.bind(this))
            .then(function() {

                key = convertedPin.charAt(1)
                this.clickKey(key).then(function($key) {
                    this.pinTextByIndex(1).should("contain.text", "*")
                }.bind(this))
                .then(function() {

                    key = convertedPin.charAt(2)
                    this.clickKey(key).then(function($key) {
                        this.pinTextByIndex(2).should("contain.text", "*")
                    }.bind(this))
                    .then(function() {

                        key = convertedPin.charAt(3)
                        this.clickKey(key).then(function($key) {
                            this.pinTextByIndex(3).should("contain.text", "*")
                        }.bind(this))
                    }.bind(this))

                }.bind(this))

            }.bind(this))

        }.bind(this))

    }

    confirmPin(pin: string): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.backButton().should("be.visible").then(function($button) {
            return this.enterPin(pin)
        }.bind(this))
    }

    login(pin: string): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.confirmPin(pin)
    }

}