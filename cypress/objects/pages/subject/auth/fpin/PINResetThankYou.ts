import ButtonPage from "../../../ButtonPage"

export default class PINResetThankYouPage extends ButtonPage {

    private imgSelector: string = "[obj-name='IEndResetLogo']"

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BEndResetNext']")
    }

    assertImgIsOnPage(): this {
        this.assertIsCurrentPage()
        cy.get(this.imgSelector).should("be.visible")

        return this
    }
}