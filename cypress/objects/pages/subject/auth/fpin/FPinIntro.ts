import ButtonPage from "../../../ButtonPage"

export default class FPinIntroPage extends ButtonPage {

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BResetPinNext']")
    }
}