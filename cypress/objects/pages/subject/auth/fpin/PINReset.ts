import PINPage from "../../../PINPage"

export default class PINResetPage extends PINPage {

    constructor(pathTail: string) {
        super(pathTail)
    }
}