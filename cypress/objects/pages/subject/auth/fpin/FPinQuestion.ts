import SingleInputErrorPage from "../../../SingleInputErrorPage"

export default class FPinQuestionPage extends SingleInputErrorPage {

    constructor(pathTail: string) {
        super(pathTail, {
            inputSelector: "[obj-name='LResetPinAnswer'] input",
            errorSelector: "[obj-name='LResetPinError']",
            submitButtonSelector: "[obj-name='BResetPinNext']"
        })
    }
}