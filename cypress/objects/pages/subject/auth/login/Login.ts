import SingleInputErrorPage from "../../../SingleInputErrorPage"

export default class LoginPage extends SingleInputErrorPage {

    constructor(pathTail: string) {
        super(pathTail, {
            inputSelector: "[obj-name='TSignInSubjectID']",
            errorSelector: "[obj-name='LSignInError']",
            submitButtonSelector: "[obj-name='SSignInLogin']"
        })
    }
}