import PINPage from "../../../PINPage"

export default class PINLoginPage extends PINPage {

    private forgotPINLinkSelector = "[obj-name='BPinResetPin']"

    constructor(pathTail: string) {
        super(pathTail)
    }

    forgotPINLink(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.forgotPINLinkSelector)
    }
}