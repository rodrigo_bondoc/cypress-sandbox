import SingleInputErrorPage from "../../../SingleInputErrorPage"

export default class SignUpPage extends SingleInputErrorPage {

    private login: string = "[obj-name='BSignUpAAccount']"

    constructor(pathTail: string) {
        super(pathTail, {
            inputSelector: "[obj-name='TSignUpCode']",
            errorSelector: "[obj-name='LSignUpErrorMsg']",
            submitButtonSelector: "[obj-name='BSignUpUnlock']"
        })
    }

    transitionToLogin(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.login).click()
    }
}