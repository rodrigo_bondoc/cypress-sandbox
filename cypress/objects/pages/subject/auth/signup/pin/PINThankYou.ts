import ButtonPage from "../../../../ButtonPage"

export default class PINThankYouPage extends ButtonPage {

    private imgSelector: string = "[obj-name='ILoggedInLogo']"

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BLoggedInNext']")
    }

    assertImgIsOnPage(): this {
        this.assertIsCurrentPage()
        cy.get(this.imgSelector).should("be.visible")

        return this
    }
}