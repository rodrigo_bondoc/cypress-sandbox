import ButtonPage from "../../../../ButtonPage"

export default class PINMismatchPage extends ButtonPage {

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BLoggedInNext']")
    }
    
}