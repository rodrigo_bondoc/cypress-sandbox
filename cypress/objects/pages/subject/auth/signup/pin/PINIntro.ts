import ButtonPage from "../../../../ButtonPage"

export default class PINIntroPage extends ButtonPage {

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BLoggedInNext']")
    }
    
}