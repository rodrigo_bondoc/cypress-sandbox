import ButtonPage from "../../../../ButtonPage"

export default class SQIntroPage extends ButtonPage {

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BLoggedInNext']")
    }
}