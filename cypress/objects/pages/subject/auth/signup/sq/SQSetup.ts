import ErrorPage from "../../../../ErrorPage"

export default class SQSetupPage extends ErrorPage {

    private dropdownBtnSelector1: string = "[obj-name='CSecurity1_BSecurityQuestionClickMask']"
    private dropdownListSelector1: string = "[obj-name='CSecurity1_LVSecurityDropdownList']"
    private answerSelector1: string = "[obj-name='CSecurity1_TBSecurityAnswerVal']"

    private dropdownBtnSelector2: string = "[obj-name='CSecurity2_BSecurityQuestionClickMask']"
    private dropdownListSelector2: string = "[obj-name='CSecurity2_LVSecurityDropdownList']"
    private answerSelector2: string = "[obj-name='CSecurity2_TBSecurityAnswerVal']"

    private submitBtnSelector: string = "[obj-name='BSecurityNext']"

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='LSecurityError']")
    }

    setUpSecurityQuestions(q1: number, q2: number): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()

        if(q1 != null && q1 >= 0) {
            cy.get(this.dropdownBtnSelector1).click().then(function($btn) {

                cy.get(this.dropdownListSelector1).should("be.visible").then(function() {
                    cy.get(this.dropdownListSelector1+" > div").each(function($elem, index, $collection) {
                        if(index == q1) {
                            cy.wrap($elem).click()
                            return false
                        }
                    })
                }.bind(this))

                cy.get(this.answerSelector1).type("1")
            }.bind(this))    
        }

        if(q2 != null && q2 >= 0) {
            cy.get(this.dropdownBtnSelector2).click().then(function($btn) {
                
                cy.get(this.dropdownListSelector2).should("be.visible").then(function() {
                    cy.get(this.dropdownListSelector2+" > div").each(function($elem, index, $collection) {
                        if(index == q2) {
                            cy.wrap($elem).click()
                            return false
                        }
                    })
                }.bind(this))

                cy.get(this.answerSelector2).type("1")
            }.bind(this))
        }

        return cy.get(this.submitBtnSelector).click().then(function() {
            return this
        }.bind(this))
    }    
}