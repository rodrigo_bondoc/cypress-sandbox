import ButtonPage from "../../ButtonPage"

export default class SplashPage extends ButtonPage {
    private continueButton: string = "[obj-name='BWelcomeContinue']"

    constructor(pathTail: string) {
        super(pathTail, "[obj-name='BWelcomeContinue']")
    }

    continue(): this {
        this.clickThrough()
        return this
    }
}