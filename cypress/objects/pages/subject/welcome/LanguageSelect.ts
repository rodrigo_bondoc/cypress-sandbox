import LanguageClient from "../../../snap-api/LanguageClient"

import Page from "../../Page"

export default class LanguageSelectPage extends Page {
    private langApi: LanguageClient

    private continueButton: string = "[obj-name='BLanguageContinue']"

    constructor(pathTail: string) {
        super(pathTail)
        this.langApi = new LanguageClient()
    }

    selectLanguage(isoLang: string): void {
        this.assertIsCurrentPage()

        this.langApi.getAllLanguages()
        .then(function(languages : object) {
            let lang: string = languages[isoLang]
            
            cy.contains(lang).click()
            .then(function() {
                cy.get(this.continueButton).click()
            }.bind(this))

        }.bind(this))
    }
}