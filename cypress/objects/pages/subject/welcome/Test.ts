import Page from "../../Page"

export default class TestPage extends Page {
    
    private clearButton: string = "[obj-name='BMainTestClearStorage']"
    private testButton: string = "[obj-name='BMainTestStart']"

    constructor(pathTail: string) {
        super(pathTail)
    }

    clearCache(): this {
        this.assertIsCurrentPage()
        cy.get(this.clearButton).click()

        return this
    }   

    testApp(): this {
        this.assertIsCurrentPage()
        cy.get(this.testButton).click()

        return this
    }
}