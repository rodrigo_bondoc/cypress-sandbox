import Page from "./Page"

export default class ButtonPage extends Page {
    
    protected buttonSelector: string
    
    constructor(pathTail: string, selector: string) {
        super(pathTail)
        this.buttonSelector = selector
    }

    button(): Cypress.Chainable<JQuery<HTMLElement>> {
        this.assertIsCurrentPage()
        return cy.get(this.buttonSelector)
    }

    clickThrough(): Cypress.Chainable<JQuery<HTMLElement>> {
        return this.button().should("be.visible").click()
    }
}