import TestPage from "../../../objects/pages/subject/welcome/Test"
import LanguageSelectPage from "../../../objects/pages/subject/welcome/LanguageSelect"
import SplashPage from "../../../objects/pages/subject/welcome/Splash"

import SignUpPage from "../../../objects/pages/subject/auth/signup/SignUp"

import LoginPage from "../../../objects/pages/subject/auth/login/Login"
import PINLoginPage from "../../../objects/pages/subject/auth/login/PINLogin"
import DashboardPage from "../../../objects/pages/subject/dashboard/Dashboard"

const loginCode = "11486720"
const loginPin = "1234"

describe("Subject Login", function() {
    const pgTest = new TestPage("SMainTest")
    const pgLanguage = new LanguageSelectPage("SLanguage")
    const pgSplash = new SplashPage("SWelcome")

    const pgSignUp = new SignUpPage("SSignUp")
    
    const pgLogin = new LoginPage("SSignIn")
    const pgPin = new PINLoginPage("SPin")
    const pgDashboard = new DashboardPage("SDashboard")

    beforeEach(function() {
        cy.visit("/studio/s3/apps/patientmainapp/index.html")
        
        pgTest.clearCache().testApp()
        pgLanguage.selectLanguage("en-US")
        
        pgSplash.continue()
        pgSignUp.assertIsCurrentPage().transitionToLogin()

        pgLogin.assertIsCurrentPage()

        cy.wait(1000)
    })

    it("Should Login Successfully", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {
            pgPin.login(loginPin)
            .then(function() {
                pgDashboard.assertIsCurrentPage()
            })
        })
    })

    it("Should Error on Empty Code", function() {
        pgLogin.enterValue(null)
        .then(function() {
            pgLogin.errorText().should("not.equal", "")
        })
    })

    it.skip("Should Error With Invalid Code", function() {
        pgLogin.enterValue("1234")
        .then(function() {
            pgPin.login(loginPin)
            .then(function() {
                pgPin.errorText().should("match", /[0-2]{1}/)
            })
        })
    })

    it("Should Error With Invalid PIN", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {
            pgPin.login("0110")
            .then(function() {
                pgPin.errorText().should("match", /[0-2]{1}/)
            })
        })
    })

    /*
        On success, account is locked for 60 seconds from the backend
        Best to use a fake or unused auth code, or do Login Test Case first
    */
    it.skip("Should Have Countdown Error After 3 Invalid Login PIN Attempts", function() {
        pgLogin.enterValue("12345678")
        .then(function() {
            pgPin.login("0000")
            .then(function() {
                pgPin.errorText().should("match", /[0-2]{1}/)
            })
        })

        pgPin.login("1111")
        .then(function() {
            pgPin.errorText().should("match", /[0-1]{1}/)
        })

        pgPin.login("2222")
        .then(function() {
            pgPin.errorText().should("match", /[0-9]{2}/)
        })
    })
})