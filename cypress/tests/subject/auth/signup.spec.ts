// Creating new patient
import MgmtUi from "../../../objects/mgmt-ui/MgmtUi"

import TestPage from "../../../objects/pages/subject/welcome/Test"
import SplashPage from "../../../objects/pages/subject/welcome/Splash"
import LanguageSelectPage from "../../../objects/pages/subject/welcome/LanguageSelect"

import SignUpPage from "../../../objects/pages/subject/auth/signup/SignUp"
import SQIntroPage from "../../../objects/pages/subject/auth/signup/sq/SQIntro"
import SQSetupPage from "../../../objects/pages/subject/auth/signup/sq/SQSetup"

import PINIntroPage from "../../../objects/pages/subject/auth/signup/pin/PINIntro"
import PINSetupPage from "../../../objects/pages/subject/auth/signup/pin/PINSetup"
import PINMismatchPage from "../../../objects/pages/subject/auth/signup/pin/PINMismatch"
import PINThankYouPage from "../../../objects/pages/subject/auth/signup/pin/PINThankYou"

const isoLang = "en-US"
const newSubjectCode = "80854265"

describe("Account Setup", function() {
    const mUi = new MgmtUi("rodrigo.bondoc+auto@snapiot.com", "#SnapMe120")

    const pgTest = new TestPage("SMainTest")
    const pgLanguage = new LanguageSelectPage("SLanguage")
    const pgSplash = new SplashPage("SWelcome")

    const pgSignUp = new SignUpPage("SSignUp")

    const pgSQIntro = new SQIntroPage("SLoggedIn")
    const pgSQ = new SQSetupPage("showScreen")

    const pgPinIntro = new PINIntroPage("showScreen")
    const pgPinSetup = new PINSetupPage("showScreen")
    const pgPinMismatch = new PINMismatchPage("SLoggedIn")
    const pgPinTy = new PINThankYouPage("showScreen")
    
    before(function() {
        // mUi.login().createPatient()
    })

    beforeEach(function() {
        cy.visit("/studio/s3/apps/patientmainapp/index.html")
        
        pgTest.clearCache().testApp()
        pgLanguage.selectLanguage("en-US")
        pgSplash.continue()
        
        // Required wait so typing in auth code into <input> will work
        cy.wait(1000)
    })

    it.skip("Should Error on Empty Code", function() {
        pgSignUp.enterValue(null)
        .then(function() {
            pgSignUp.errorText().should("match", /[^0-9]/)
        })
    })    

    it.skip("Should Error After Invalid Code", function() {
        pgSignUp.enterValue("1234")
        .then(function() {
            pgSignUp.errorText().should("match", /[^0-9]/)
        })
    })

    it.skip("Should Have Countdown Error After 3 Invalid Codes", function() {

        pgSignUp.enterValue("1")
        .then(function() {
            pgSignUp.errorText().should("match", /[^0-9]/)
        })

        pgSignUp.enterValue("1234567")
        .then(function() {
            pgSignUp.errorText().should("match", /[^0-9]/)
        })

        pgSignUp.enterValue("98765432")
        .then(function() {
            pgSignUp.errorText().should("match", /[0-9]/)
        })

    })

    it.skip("Should Error on Empty Security Questions", function() {
        pgSignUp.enterValue(newSubjectCode)
        .then(function() {

            pgSQIntro.clickThrough()
            .then(function() {

                pgSQ.setUpSecurityQuestions(null, null)
                .then(function() {

                    pgSQ.errorText().should("not.equal", "")
                })
            })
        })
    })

    it.skip("Should Error on Same Security Questions", function() {
        pgSignUp.enterValue(newSubjectCode)
        .then(function() {

            pgSQIntro.clickThrough()
            .then(function() {

                pgSQ.setUpSecurityQuestions(0, 0)
                .then(function() {

                    pgSQ.errorText().should("not.equal", "")
                })
            })
        })
    })

    it("Should Error when Confirming with Different PIN", function() {
        pgSignUp.enterValue(newSubjectCode)
        .then(function() {

            pgSQIntro.clickThrough()
            .then(function() {

                pgSQ.setUpSecurityQuestions(0, 1)
                .then(function() {

                    pgPinIntro.clickThrough().then(function() {

                        pgPinSetup.enterPin("1211")
                        .then(function() {

                            pgPinSetup.confirmPin("2222")
                            .then(function() {

                                pgPinMismatch.assertIsCurrentPage()
                            })
                        })
                    })
                })
            })
        })
    })
    
    it.skip("Should Complete Sign Up", function() {

        pgSignUp.enterValue(newSubjectCode)
        .then(function() {

            pgSQIntro.clickThrough()
            .then(function() {

                pgSQ.setUpSecurityQuestions(0, 1)
                .then(function() {

                    pgPinIntro.clickThrough().then(function() {

                        pgPinSetup.enterPin("1111")
                        .then(function() {

                            pgPinSetup.confirmPin("1111")
                            .then(function() {

                                pgPinTy.assertIsCurrentPage().assertImgIsOnPage()
                            })
                        })
                    })
                })
            })
        })
    }) 
})