import TestPage from "../../../objects/pages/subject/welcome/Test"
import LanguageSelectPage from "../../../objects/pages/subject/welcome/LanguageSelect"
import SplashPage from "../../../objects/pages/subject/welcome/Splash"

import SignUpPage from "../../../objects/pages/subject/auth/signup/SignUp"
import PINMismatchPage from "../../../objects/pages/subject/auth/signup/pin/PINMismatch"

import LoginPage from "../../../objects/pages/subject/auth/login/Login"
import PINLoginPage from "../../../objects/pages/subject/auth/login/PINLogin"

import FPinIntroPage from "../../../objects/pages/subject/auth/fpin/FPinIntro"
import FPinQuestionPage from "../../../objects/pages/subject/auth/fpin/FPinQuestion"
import PINResetPage from "../../../objects/pages/subject/auth/fpin/PINReset"
import PINResetThankYouPage from "../../../objects/pages/subject/auth/fpin/PINResetThankYou"

const loginCode = "11486720"

describe("", function() {
    const pgTest = new TestPage("SMainTest")
    const pgLanguage = new LanguageSelectPage("SLanguage")
    const pgSplash = new SplashPage("SWelcome")

    const pgSignUp = new SignUpPage("SSignUp")
    
    const pgLogin = new LoginPage("SSignIn")
    const pgPin = new PINLoginPage("SPin")

    const pgFPinIntro = new FPinIntroPage("SResetPin")
    const pgFPinQuestion = new FPinQuestionPage("showScreen")
    const pgPinReset = new PINResetPage("showScreen")
    const pgPinMismatch = new PINMismatchPage("showScreen")
    const pgPinTy = new PINResetThankYouPage("SEndResetPin")


    beforeEach(function() {
        cy.visit("/studio/s3/apps/patientmainapp/index.html")
        
        pgTest.clearCache().testApp()
        pgLanguage.selectLanguage("en-US")
        
        pgSplash.continue()
        pgSignUp.assertIsCurrentPage().transitionToLogin()

        pgLogin.assertIsCurrentPage()

        cy.wait(1000)
    })

    it.skip("Question 1: Should Error on Empty Answer", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("")
                    .then(function() {
                        pgFPinQuestion.errorText().should("contain", "Please enter an answer")
                    })
                })
            })
        })
    })    

    it.skip("Question 1: Should Error on Invalid Answer", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue(66)
                    .then(function() {
                        pgFPinQuestion.errorText().should("match", /[2]{1}/)
                    })
                    .then(function() {
                        
                        pgFPinQuestion.enterValue(66)
                        .then(function() {
                            pgFPinQuestion.errorText().should("match", /[1]{1}/)
                        })
                    })
                })
            })
        })
    })

    it.skip("Question 1: Should Countdown Error After 3 Invalid Answers", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue(66)
                    .then(function() {
                        pgFPinQuestion.errorText().should("match", /[2]{1}/)
                    })
                    .then(function() {
                        
                        pgFPinQuestion.enterValue(66)
                        .then(function() {
                            pgFPinQuestion.errorText().should("match", /[1]{1}/)
                        })
                        .then(function() {
                            
                            pgFPinQuestion.enterValue(66)
                            .then(function() {
                                pgFPinQuestion.errorText().should("match", /[0-9]{2}/)
                                .then(function() {
                                    cy.wait(2000)
                                    pgFPinQuestion.errorText().should("match", /[0-7]{2}/)
                                })
                            })
                        })
                    })
                })
            })
        })
    }) 
    
    it.skip("Question 2: Should Error on Empty Answer", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue("")
                        .then(function() {
                            pgFPinQuestion.errorText().should("contain", "Please enter an answer")
                        })
                    })
                })
            })
        })
    })    
    
    it.skip("Question 2: Should Error on Invalid Answer", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue(66)
                        .then(function() {
                            pgFPinQuestion.errorText().should("match", /[2]{1}/)
                        })
                        .then(function() {
                            
                            pgFPinQuestion.enterValue(66)
                            .then(function() {
                                pgFPinQuestion.errorText().should("match", /[1]{1}/)
                            })
                        })
                    })
                })
            })
        })
    })
    
    it.skip("Question 2: Should Countdown Error After 3 Invalid Answers", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue(66)
                        .then(function() {
                            pgFPinQuestion.errorText().should("match", /[2]{1}/)
                        })
                        .then(function() {
                            
                            pgFPinQuestion.enterValue(66)
                            .then(function() {
                                pgFPinQuestion.errorText().should("match", /[1]{1}/)
                            })
                            .then(function() {
                                
                                pgFPinQuestion.enterValue(66)
                                .then(function() {
                                    pgFPinQuestion.errorText().should("match", /[0-9]{2}/)
                                    .then(function() {
                                        cy.wait(2000)
                                        pgFPinQuestion.errorText().should("match", /[0-7]{2}/)
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })    
    
    it.skip("Should Move to Reset PIN", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue("4")
                        .then(function() {
                            pgPinReset.assertIsCurrentPage()
                        })
                    })
                })
            })
        })
    })
    
    it.skip("Should Error on PIN Mismatch", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue("4")
                        .then(function() {
                            pgPinReset.enterPin("1111").then(function() {
                                pgPinReset.confirmPin("2222").then(function() {
                                    pgPinMismatch.assertIsCurrentPage()
                                })
                            })
                        })
                    })
                })
            })
        })
    })

    it.skip("Should Error on Old PIN", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue("4")
                        .then(function() {
                            pgPinReset.enterPin("1234").then(function() {
                                pgPinReset.confirmPin("1234").then(function() {
                                    pgPinReset.errorText().should("contain", "Cannot reuse old PIN. Try again.")
                                })
                            })
                        })
                    })
                })
            })
        })
    })
    
    it.skip("Should Reset PIN Successfully", function() {
        pgLogin.enterValue(loginCode)
        .then(function() {

            pgPin.forgotPINLink().should("be.visible").click()
            .then(function() {

                pgFPinIntro.clickThrough()
                .then(function() {

                    pgFPinQuestion.enterValue("4")
                    .then(function() {

                        // Required wait to let page load to 2nd question
                        cy.wait(1000)
                        pgFPinQuestion.enterValue("4")
                        .then(function() {
                            pgPinReset.enterPin("5552").then(function() {
                                pgPinReset.confirmPin("5552").then(function() {
                                    pgPinTy.assertIsCurrentPage().clickThrough().then(function() {

                                        // App goes here on a fresh start
                                        pgLanguage.assertIsCurrentPage()
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })     
})