/* 
describe(groupName, callback)
    <string> groupName          Name of the test group
    <function> callback         Callback function

    Simply a way to group tests together in Mocha

    describe() calls can be nested within another describe() call

it(caseName, testCallback)
    <string> caseName           Name explaining what the test does
    <function> testCallback     Actual test code

    Used for an individual test case
*/

describe("My First Test", function() {
    it("Assert True", function() {
        expect(true).to.equal(true)
    })

    it.skip("Fail Me", function() {
        expect(true).to.equal(false)
    })
})

const snapUrl = "https://snapiot.com/"

describe("Find SnapIoT", function() {
    it("Visit Google", function() {
        cy.visit("https://www.google.com/")
        cy.get(".gLFyf")
            .type("SnapIoT")

        cy.contains("Google Search").click()

        cy.contains("snapIoT | Digital Clinical Trials")
            .should("have.attr", "href", snapUrl)
    })

    it("Visit SnapIoT", function() {
        cy.visit(snapUrl)
        
        cy.contains("Company").click()

        cy.url()
            .should("include", "company.html")

        cy.contains("Our Background")
            .scrollIntoView()
    })
})