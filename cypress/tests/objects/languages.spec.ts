import TestPage from "../../objects/pages/subject/welcome/Test"
import SplashPage from "../../objects/pages/subject/welcome/Splash"
import LanguageSelectPage from "../../objects/pages/subject/welcome/LanguageSelect"

/*
Testing sometimes flaky test where the server returns a 500 error randomly?

Changed Client to retest on failed HTTP responses
*/
describe("Language Select Test", function() {
    const pgTest = new TestPage("SMainTest")
    const pgLanguage = new LanguageSelectPage("SLanguage")
    const pgSplash = new SplashPage("SWelcome")

    beforeEach(function() {
        cy.visit("/studio/s3/apps/patientmainapp/index.html")
        pgTest.clearCache().testApp()
    })

    Cypress._.times(10, (i) => {
        it(`Run #${i + 1} - Test Language API Call and Language Selection Screen`, () => {
            pgLanguage.selectLanguage("en-US")
            pgSplash.continue()
        })
      })
})