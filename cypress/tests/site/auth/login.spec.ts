import SplashPage from "../../../objects/pages/site/welcome/Splash"
import LoginPage from "../../../objects/pages/site/login/Login"
import DashboardPage from "../../../objects/pages/site/dashboard/Dashboard"

const validEmail = "rodrigo.bondoc+auto@snapiot.com"
const validPassword = "#SnapMe120"

describe("Site Login", function() {
    const pgSplash = new SplashPage("SMainScreen")
    const pgLogin = new LoginPage("SLogin")
    const pgDashboard = new DashboardPage("showScreen")

    beforeEach(function() {
        cy.visit("/studio/s3/apps/sitemainapp/index.html")

        pgSplash.continue()
    })

    it("Should Login Successfully", function() {
        pgLogin.loginWith(validEmail, validPassword).then(function() {
            pgDashboard.assertIsCurrentPage()
        })
    })

    it("Should Error on Empty Credentials", function() {
        pgLogin.loginWith(null, null).then(function() {
            pgLogin.errorMessage().should("not.equal", "")
        })
    })

    it("Should Get Attempt Errors on Invalid Logins", function() {
        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-3]{1}/)
        })

        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-2]{1}/)
        })

        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-1]{1}/)
        })
    })

    it("Should Get Maximum Attempt Error on 4 Invalid Logins", function() {
        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-3]{1}/)
        })

        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-2]{1}/)
        })

        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("match", /[0-1]{1}/)
        })

        pgLogin.loginWith("hm"+validEmail, validPassword).then(function() {
            pgLogin.errorMessage().should("contain", "You have reached the maximum number of attempts.")
        })
    })
})